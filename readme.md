## Installation
* This project uses python3 and Django 2.1, and is highly mobile responsive too.
* Clone this repository using `git clone` command.
* Create virtualenv of any name you like and activate it.
* Go to the cloned repository root folder and run `pip install -r requirements.txt`
* To run the project, `python manage.py runserver`
* Make sure to create superuser to access django admin console and create capacity using ConfigurationModel
* To run tests, `coverage run --source='.' manage.py test && coverage report`. Right now the project has 94% test coverage.

## Functionality
* User could sign up as customer. User cant book a slot until he/she is registered.
* Super user is an admin, it has to be created and only that person could add configuration, such as capacity on any given date or from date till to date. This could be done in django admin console.
* Configuration to add capacity cant be added again for specific given dates which are already present. For ex, if admin has already set capacity for 30/01/2019 till 01/02/2019, than admin cant add dates like, 30/01/2019 till 31/01/2019 or even 30/01/2019 till 02/02/2019. As it will conflict with business logic. 
* User can only book by one date
* User cannot delete the booked slots now
* User cant update the booked slot now as well

## Future considerations
* Admin should be a separate user type, and so should be the customer
* User could update/delete the bookings
* Would be good to show available slots for the next 7 days in the front page
* Superadmin access should not be given to anyone