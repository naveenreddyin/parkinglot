from django.apps import AppConfig


class ParkinglotAuthConfig(AppConfig):
    name = 'parkinglot_auth'
