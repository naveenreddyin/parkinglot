from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.contrib.auth import get_user_model


class SignUpForm(UserCreationForm):
    phone_number = forms.CharField(max_length=50)
    first_name = forms.CharField(max_length=60)
    last_name = forms.CharField(max_length=60)
    
    class Meta(UserCreationForm.Meta):
        model = get_user_model()

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.phone_number = self.cleaned_data.get('phone_number')
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.save()
        return user