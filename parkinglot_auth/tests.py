from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from utils.testutils import BaseTest


class ParkinglotAuth(BaseTest):
    """
    This class is to test auth related urls.
    """

    def test_user_model(self):
        """
        This is to test our custom user model
        """
        self.assertEqual(self.usermodel.objects.all().count(), 0)
        self.user_creation_util()
        self.assertEqual(self.usermodel.objects.all().count(), 1)
        user1 = self.usermodel.objects.get(username="user1")
        self.assertTrue(hasattr(user1, "phone_number"))
        self.assertEqual(user1.phone_number, "+312222222")
        self.assertEqual(user1.first_name, "Naveen")
        self.assertEqual(user1.last_name, "Reddy")

    def test_login_url(self):
        """
        This function will test the login url, if its present than 200 status code else 404.
        404 status code means the login url is not configured.
        We use django.contrib.auth.urls which has many things inbuilt.
        """
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        # check if has form in context
        self.assertIsNotNone(response.context['form'])
        self.user_creation_util()
        response = self.client.post(
            reverse('login'), {'username': 'user1', 'password': 'user1'}, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEqual(response.context['user'].first_name, "Naveen")
        self.assertRedirects(response, "{}".format(
            reverse('console:index')))

    def test_logout_url(self):
        """
        This testcase will test the logout url.
        It should redirect and give status code 302.
        """
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "{}".format(
            reverse('index')))

    def test_user_registration_view(self):
        """
        This will test the user signup/registration process.
        """
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/signup.html")
        # try to create an user
        response = self.client.post(reverse('signup'), {'username': 'user1', 'password1': 'testuser',
                                                        'password2': 'testuser',
                                                        'first_name': 'Naveen', 'last_name': 'Reddy',
                                                        'phone_number': '+4795174867'},
                                    follow=True)
        self.assertTrue(response.status_code, 201)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEqual(response.context['user'].first_name, "Naveen")
        self.assertEqual(self.usermodel.objects.all().count(), 1)
