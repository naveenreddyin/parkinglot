from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.contrib.auth import get_user_model
from django.contrib.auth import login

from parkinglot_auth.forms import SignUpForm

class SignUpView(CreateView):
    model = get_user_model()
    form_class = SignUpForm
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('index')