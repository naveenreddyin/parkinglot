from django.test import TestCase
from django.urls import reverse

from utils.testutils import BaseTest


class FrontendTest(BaseTest):

    def test_index_url(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/index.html")
