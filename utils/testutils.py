from django.test import TestCase
from django.contrib.auth import get_user_model
from django.test import Client


class BaseTest(TestCase):

    def setUp(self):
        self.usermodel = get_user_model()
        self.client = Client()

    def user_creation_util(self, username=None):
        user = self.usermodel.objects.create_user(username='user1' if not username else username,
                                                  email='user1@dispostable.com',
                                                  password='user1',
                                                  first_name="Naveen",
                                                  last_name="Reddy",
                                                  phone_number="+312222222")
        return user
