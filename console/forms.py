from django.forms import ModelForm, forms
from django.db import transaction
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from console.models import BookingModel, ConfigurationModel


class BookingForm(ModelForm):

    class Meta:
        model = BookingModel
        fields = ('date_and_time', )

    @transaction.atomic
    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.save()
        return instance

    def clean(self):
        cleaned_data = super().clean()
        date_and_time = cleaned_data.get("date_and_time")

        if not date_and_time:
            raise ValidationError('Date and Time cannot be empty.')

        if not ConfigurationModel.objects.filter(from_date__lte=date_and_time, to_date__gte=date_and_time).exists():
            raise ValidationError(
                'There are no dates available on the selected date and time by you.'
                'Administrator has not added any dates.')

        configuration = ConfigurationModel.objects.get(
            from_date__lte=date_and_time, to_date__gte=date_and_time)
        bookings = BookingModel.objects.filter(
            date_and_time__date=date_and_time.date()).count()
        if bookings == configuration.capacity:
            raise ValidationError(
                'We do not have more capacity to book you at this time. Please try again.')
