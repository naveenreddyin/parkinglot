from django.contrib import admin

from console.models import BookingModel, ConfigurationModel


admin.site.register(BookingModel)
admin.site.register(ConfigurationModel)
