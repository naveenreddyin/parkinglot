from django.urls import path

from console.views import BookingView

urlpatterns = [
    path('', BookingView.as_view(), name="index"),
]