from django.db import models
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError


class BookingModel(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    date_and_time = models.DateTimeField()

    class Meta:
        verbose_name = ('Booking')
        verbose_name_plural = ('Bookings')

    def __str__(self):
        return "User : {}, has booked date time slot, {}".format(self.user.username,
                                                                 self.date_and_time)


class ConfigurationModel(models.Model):
    capacity = models.IntegerField()
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()

    class Meta:
        verbose_name = ('Configuration')
        verbose_name_plural = ('Configurations')

    def __str__(self):
        return "Capacity : {}, from: {}, till: {}".format(self.capacity,
                                                          self.from_date, self.to_date)

    def clean(self, *args, **kwargs):
        if self.from_date >= self.to_date:
            raise ValidationError(
                'From date should not be greater than to date. From date was {} and to date was {}'.format(self.from_date,
                                                                                                           self.to_date))
        if self.pk is None:
            if ConfigurationModel.objects.filter(Q(from_date__date__gte=self.from_date) & Q(to_date__date__lte=self.to_date) | Q(from_date__date__lte=self.from_date) & Q(to_date__date__gte=self.to_date)).count() > 0:
                raise ValidationError(
                    'You already have configurations for those days, please modify it or delete it.')
        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        # raise
        self.full_clean()
        super().save(*args, **kwargs)
