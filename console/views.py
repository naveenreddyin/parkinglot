from django.shortcuts import reverse, redirect
from django.views.generic import CreateView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin

from console.models import BookingModel
from console.forms import BookingForm


class BookingView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = BookingModel
    form_class = BookingForm
    template_name = 'console/index.html'
    success_url = '/console/'
    success_message = "Booking was added successfully"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Get all the bookings by user
        context['bookings'] = BookingModel.objects.filter(user=self.request.user)
        return context


    def form_valid(self, form):
        instance = form.instance
        instance.user = self.request.user
        instance.save()
        return super().form_valid(form)
