from django.test import TestCase
from django.urls import reverse
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from django.db.models.query import QuerySet

from console.forms import BookingForm
from console.models import BookingModel, ConfigurationModel
from utils.testutils import BaseTest


class ConsoleTest(BaseTest):

    def test_booking_model(self):
        """
        This test is for the BookingModel
        """
        self.assertEqual(BookingModel.objects.all().count(), 0)

        user = self.user_creation_util()
        now = datetime.now()
        BookingModel.objects.create(
            user=user, date_and_time=now)
        self.assertEqual(BookingModel.objects.all().count(), 1)

    def test_configuration_model(self):
        """
        This is to test ConfigurationModel, which will be used to set the parking lot capacity for any given day.
        This for now is to be set on django admin site, by super user, but could easily be transformed into console as
        well.
        ConfigurationModel has three fields, capacity, from_date, to_date. Admin could add multiple of such capcities,
        but it will check against the saved db instances if something already falls under that from_date and to_date range.
        If it falls than it wont save.
        """
        self.assertEqual(ConfigurationModel.objects.all().count(), 0)
        # save with normal clean data
        ConfigurationModel.objects.create(capacity=100, from_date=datetime.now(),
                                          to_date=datetime.now() + timedelta(hours=2))
        self.assertEqual(ConfigurationModel.objects.all().count(), 1)
        # Now try to save with to_date less than from_date, should throw exception
        with self.assertRaises(ValidationError) as exception_context_manager:
            ConfigurationModel.objects.create(capacity=100, from_date=datetime.now(
            ), to_date=datetime.now() - timedelta(hours=2))

        # Now test to create instance by date range already present, we already have one db entry, we will test against
        # that date range
        self.assertEqual(ConfigurationModel.objects.all().count(), 1)
        with self.assertRaises(ValidationError) as exception_context_manager:
            ConfigurationModel.objects.create(capacity=100, from_date=datetime.now(
            ), to_date=datetime.now() + timedelta(hours=3))

    def test_booking_form(self):
        """
        This is test case for BookingForm.
        1. Test to see if an error raises when date doesnt exist in database, i.e not entered by admin using ConfigurationModel.
        3. Test with normal case.
        """
        data = {
            'user': self.user_creation_util(),
            'date_and_time': datetime.now()
        }
        form = BookingForm(data)
        self.assertFalse(form.is_valid())
        # create Configuration
        ConfigurationModel.objects.create(capacity=1, from_date=datetime.now() - timedelta(days=2),
                                          to_date=datetime.now() + timedelta(days=2))
        form = BookingForm(data)
        self.assertTrue(form.is_valid())
        


    def test_access_console_urls(self):
        """
        This test is to check if all the urls of console app requires login required.
        If we get response 302 without login, it means the view require login first.
        """
        # Try and access root
        response = self.client.get(reverse('console:index'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "{}?next={}".format(
            reverse('login'), reverse('console:index')))
        # login
        self.user_creation_util()
        self.client.login(username='user1', password='user1')
        response = self.client.get(reverse('console:index'))
        self.assertIsInstance(response.context.get('form'), BookingForm)
        self.assertIsInstance(response.context[-1]['bookings'], QuerySet)
        # test with post form
        data = {
            'user': self.user_creation_util('user2'),
            'date_and_time': datetime.now(),
        }
        response = self.client.post(reverse('console:index'), data=data)
        self.assertIsNotNone(response.context['form'].errors)
        self.assertEqual(BookingModel.objects.all().count(), 0)
        # create Configuration
        ConfigurationModel.objects.create(capacity=1, from_date=datetime.now() - timedelta(days=2),
                                          to_date=datetime.now() + timedelta(days=2))
        response = self.client.post(reverse('console:index'), data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(BookingModel.objects.all().count(), 1)
        # Now it should not accept the data as we have capacity set to 1
        response = self.client.post(reverse('console:index'), data=data)
        self.assertIsNotNone(response.context['form'].errors)
        self.assertEqual(BookingModel.objects.all().count(), 1)
