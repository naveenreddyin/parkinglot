# Generated by Django 2.1.5 on 2019-01-27 23:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('console', '0002_configurationmodel'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bookingmodel',
            old_name='from_time',
            new_name='date_and_time',
        ),
        migrations.RemoveField(
            model_name='bookingmodel',
            name='to_time',
        ),
    ]
